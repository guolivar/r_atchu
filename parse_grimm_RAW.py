#!/usr/bin/env python
import datetime
# Read settings file
settings_file = open('./grimm_settings.txt')
input_file = settings_file.readline().rstrip()
output_file = settings_file.readline().rstrip()
model = settings_file.readline().rstrip()
sizes_list = settings_file.readline().rstrip()
# If it is an invalid record ...put an invalid data line
if model == '107':
	pad_line = 'NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN'
if model == '108':
	pad_line = 'Nan;NaN;NaN'
if model == '109':
	pad_line = 'NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN'
parsed_file = open(output_file,'w')
parsed_file.write('date;error;' + sizes_list + '\n')
with open(input_file,'r') as datafile:
	while True:
		if model == '107':
			line = datafile.readline()
			if not line: break
			if len(line)<30: continue
			while ((line) and (line[0]!='P')):
				line = datafile.readline()
				if len(line)<30:break
			if len(line)<30: continue
			if not line: break
			#The starting point is a P line that's already in line
			#Get date, time and error code
			#idx1 = line.find(',')
			c_vec = line[3:].split()
			p_vec = map(int,c_vec)
			cdate = datetime.datetime(p_vec[0]+2000,p_vec[1],p_vec[2],p_vec[3],p_vec[4],0)
			c_timestamp = cdate.strftime("%Y/%m/%d %H:%M:%S")
			output_line = c_timestamp + ';' + c_vec[7] + ';'
			print(output_line)
			invalid = 0
			# Get 2 C lines and populate tha data lists
			# First line
			line = datafile.readline()
			if not line: break
			if (line[0]=='K'):line = datafile.readline();line = datafile.readline()
			if not line: break
			if ((line[0]=='C') and (len(line)>65)):
				d_vec = line[2:].split()
				output_line = output_line + ';'.join(d_vec[0:7])
			else:
				invalid = 1
			# Second line
			line = datafile.readline()
			if not line: break
			if ((line[0]=='c') and (len(line)>65)):
				d_vec = line[2:].split()
				output_line = output_line + ';' + ';'.join(d_vec)
			else:
				invalid = 1
		if model == '108':
			# Not supported for now so duplicate the input to the output
			output_line = datafile.readline()
		if model == '109':
			line = datafile.readline()
			print(line)
			if not line: break
			if len(line)<30: continue
			while ((line) and (line[0]!='P')):
				line = datafile.readline()
				if len(line)<30:break
			if len(line)<30: continue
			if not line: break
			#The starting point is a P line that's already in line
			#Get date, time and error code
			#idx1 = line.find(',')
			c_vec = line[3:].split()
			p_vec = map(int,c_vec)
			cdate = datetime.datetime(p_vec[0]+2000,p_vec[1],p_vec[2],p_vec[3],p_vec[4],0)
			c_timestamp = cdate.strftime("%Y/%m/%d %H:%M:%S")
			output_line = c_timestamp + ';' + c_vec[7] + ';'
			print(output_line)
			invalid = 0
			# Get 2 C and 2 c lines to populate tha data lists
			# First line
			line = datafile.readline()
			if not line: break
			if (line[0]=='K'):line = datafile.readline();#line = datafile.readline()
			if not line: break
			print(line)
			if ((line[0]=='C') and (len(line)>65)):
				d_vec = line[2:].split()
				output_line = output_line + ';'.join(d_vec[1:9])
			else:
				invalid = 1
			# Second line
			line = datafile.readline()
			if not line: break
			if ((line[0]=='C') and (len(line)>65)):
				d_vec = line[2:].split()
				output_line = output_line + ';' + ';'.join(d_vec[1:8])
			else:
				invalid = 1
			line = datafile.readline()
			if not line: break
			if ((line[0]=='c') and (len(line)>65)):
				d_vec = line[2:].split()
				output_line = output_line + ';' + ';'.join(d_vec[1:9])
			else:
				invalid = 1
			# Second line
			line = datafile.readline()
			if not line: break
			if ((line[0]=='c') and (len(line)>65)):
				d_vec = line[2:].split()
				output_line = output_line + ';' + ';'.join(d_vec[1:9])
			else:
				invalid = 1
		if invalid:
			#There were not 1x2 (or 1x4) lines of data, i.e., not a full sample
			#So, print an invalid record
			parsed_file.write(c_timestamp + ';' + pad_line + '\n')
		else:
			#We have a full period so print the record to file
			parsed_file.write(output_line + '\n')
parsed_file.close()
datafile.close()
