# NOx - 2B ####
# Load packages
library('parallel')
library('ggplot2')
library('openair')
library('lubridate')
# Load data ####
nox_2B <- read.csv("~/data/ATCHU/UoA/2BNOX/parsed2B.txt", stringsAsFactors=FALSE)
nox_2B$date<-as.POSIXct(nox_2B$date,tz='GMT')
nox_2B$date = nox_2B$date+12*3600
# Plotting Summaries ####
png("./nox_time_series.png",width = 1200,height = 600)
timePlot(mydata = nox_2B,pollutant = names(nox_2B)[2:9],avg.time = '1 hour')
dev.off()