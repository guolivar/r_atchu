clear all
# fclose all
# close all
# Read the pasted GRIMM file
page_screen_output(0);
page_output_immediately(1);
# Set the scene
work_dir='/home/olivaresga/Software/CoolTermLinux/';
work_file='grimm_silver_deepwave_RAW.txt';
fixed_file=[work_file(1:end-4) '_fix.txt'];
id_in=fopen([work_dir work_file],'rt');
id_ou=fopen([work_dir fixed_file],'w');
# Write headers
first_line={'OctaveDate','Year','Month','Day','Hour','Minute','Second','Error', ...
  '300', ...
'400', ...
'500', ...
'650', ...
'800', ...
'1000', ...
'1600', ...
'2000', ...
'3000', ...
'4000', ...
'5000', ...
'7500', ...
'10000', ...
'15000', ...
'20000'};
dp=[0.30 0.40 0.50  0.65 0.80 1.00 1.60 2.00  3.0 4.0 5.0 7.5 10.0 15.0 20.0];
dlogDp=[0.124939   0.096910   0.113943   0.090177   0.096910   0.204120   0.096910   0.176091   0.124939   0.096910   0.176091   0.124939   0.176091   0.124939 1];
fmt_hdr='%s';
for i=1:21
  fmt_hdr=[fmt_hdr '\t%s'];
end
fmt_hdr=[fmt_hdr '\t%s\n'];
fmt_data='%f\t%d\t%d\t%d\t%d\t%d\t%d';
for i=1:14
  fmt_data=[fmt_data '\t%f'];
end
fmt_data=[fmt_data '\t%f\n'];
pad_data=NaN*ones(1,15);
fprintf(id_ou,fmt_hdr,first_line{:});
# Read the first lines until encountering a P line
c_line=fgetl(id_in);
while and(~feof(id_in),c_line(1)!='P')
  c_line=fgetl(id_in);
  c_line
end
while ~feof(id_in)
  #The starting point is a P line that's already in c_line
  #Get date, time and error code
  p_vec=sscanf(c_line(4:end),'%f')';
  octave_date=datenum([p_vec(1:5) 0]);
  data_vec1=[octave_date p_vec(1:5) p_vec(8)];
  #Get the Number concentration lines (C records)for the whole period
  next_P=(0==1);
  invalid=0;
  data_vec2=zeros(1,15);
  for imin=1:1
    #get 2 lines of data and populate the data vector
    #1
    if ~feof(id_in)
      c_line=fgetl(id_in);
    else
      invalid=1;
      break;
    end
    if and(c_line(1)=='C',length(c_line)>65)
      data_vec2(1:8)=data_vec2(1:8)+sscanf(c_line(4:74),'%f')';
    else
      invalid=1;
      break;
    end
    #2
    if ~feof(id_in)
      c_line=fgetl(id_in);
    else
      invalid=1;
      break;
    end
    if and(c_line(1)=='c',length(c_line)>65)
      #This line has the repeated 2000nm channel
      data_vec2(9:15)=data_vec2(9:15)+sscanf(c_line(4:65),'%f')';
    else
      invalid=1;
      break;
    end
  end
  if invalid
    #There were not 1x2 lines of data, i.e., not a full sample
    #So, print an invalid record
    fprintf(id_ou,fmt_data,[data_vec1 pad_data]);
  else
    #We have a full period so print the record to file
    #First, we calculate the dM/dlogDp for each channel,
    #except the last one that keeps as is
    data_vec2(1:14)=data_vec2(1:14)-data_vec2(2:15);
    data_vec2=data_vec2./dlogDp;
    fprintf(id_ou,fmt_data,[data_vec1 data_vec2]);
  end
  #Now look for the next P line
  next_P=0;
  while and(~feof(id_in),~next_P)
    c_line=fgetl(id_in);
    if length(c_line)>65
      next_P=c_line(1)=='P';
    else
      next_P==0;
    end
  end
end
fclose(id_in);
fclose(id_ou);


# Pad a pasted file according to the timestep selected
# Missing data will have a timestamp but NaN in the data
fclose all
clear
page_screen_output(0);
page_output_immediately(1);
# Set the scene
work_dir='/home/olivaresga/Software/CoolTermLinux/';
work_file='grimm_silver_deepwave_RAW_fix.txt';
fixed_file=[work_file(1:end-8) '_full.txt'];
id_in=fopen([work_dir work_file],'rt');
id_ou=fopen([work_dir fixed_file],'w');
# pad record ... no timestamp
pad_vec=NaN*ones(1,16);
fmt_data='%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d';
for i=1:14
  fmt_data=[fmt_data '\t%f'];
end
fmt_data=[fmt_data '\t%f\n'];
# Timestep expected ... in vector form [Yr Mo Dy Hr Mi Se]
timestep=[0 0 0 0 10 0];
# Write headers
t_line=fgetl(id_in);
fprintf(id_ou,'%s\n',t_line);
# Read first line and set starting date
t_line=fgetl(id_in);
data_vec=sscanf(t_line,'%f')';
curr_date_num=datenum([data_vec(2:6) 0]+timestep);
fprintf(id_ou,'%s\t',datestr(curr_date_num,"20yy-mm-dd HH:MM:SS"))
fprintf(id_ou,fmt_data,[data_vec(1:6) 0 data_vec(7:end)]);
datestr(curr_date_num)
next_report=curr_date_num+1;
while ~feof(id_in)
  #read next data line
  t_line=fgetl(id_in);
  data_vec=sscanf(t_line,'%f')';
  #get DATE to which this record belongs
  next_date_num=datenum([data_vec(2:6) 0]);
  #Compare the record_date with the expected date. If the record date is into the future,
  #output a PAD line to the file and increase expected date value.
  while next_date_num>=curr_date_num
    fprintf(id_ou,'%s\t',datestr(curr_date_num,"20yy-mm-dd HH:MM:SS"))
    fprintf(id_ou,fmt_data,[curr_date_num datevec(curr_date_num) pad_vec]);
    curr_date_num=curr_date_num+datenum(timestep);
    'missing data'
  end
  fprintf(id_ou,'%s\t',datestr(curr_date_num,"20yy-mm-dd HH:MM:SS"))
  fprintf(id_ou,fmt_data,[curr_date_num datevec(curr_date_num) data_vec(7:end)]);
  curr_date_num=curr_date_num+datenum(timestep);
  if curr_date_num>next_report
    datestr(curr_date_num)
    next_report=next_report+1;
  end
end
fclose(id_in)
fclose(id_ou)

#Now load the relevant data and save the HDF5 file
grimm_data1=dlmread([work_dir fixed_file],'\t',1,0);
grimm_data1(find(grimm_data1(:,8)!=0),9:end)=NaN;
nrecs=length(grimm_data1(:,1));
npars=length(grimm_data1(1,:));
# Original size bins
grimm_dp=[0.30 0.40 0.50  0.65 0.80 1.00 1.60 2.00  3.0 4.0 5.0 7.5 10.0 15.0 20.0];
grimm_data=[grimm_data1(:,1:16) grimm_data1(:,17:npars)];
grimm_dlogDp=[0.124939   0.096910   0.113943   0.090177   0.096910   0.204120   0.096910   0.096910   0.079181   0.124939   0.096910   0.176091   0.124939   0.176091   0.124939];
grimm_headers={'POSIXdate','OctaveDate','Year','Month','Day','Hour','Minute','Second', 'Error', ...
  '300', ...
  '400', ...
  '500', ...
  '650', ...
  '800', ...
  '1000', ...
  '1600', ...
  '2000', ...
  '3000', ...
  '4000', ...
  '5000', ...
  '7500', ...
  '10000', ...
  '15000', ...
  '20000'};
# Calculate PM10, PM2.5 and PM1 time series
# pm_series=NaN*ones(nrecs,9);
# pm_series(:,1:6)=grimm_data(:,1:6);
# for rec=1:nrecs
#   pm1=trapz(grimm_dp(1:6),grimm_data(rec,9:14));
#   pm2_5=trapz(grimm_dp(1:8),grimm_data(rec,9:16));
#   pm10=trapz(grimm_dp(1:14),grimm_data(rec,9:22));
#   pm_series(rec,7:9)=[pm1 pm2_5 pm10];
# end
save('-hdf5',[work_dir 'grimm_data.h5'],'grimm_headers','grimm_dp','grimm_dlogDp','grimm_data');
figure(1);
plot(grimm_data(:,1),grimm_data(:,9),'-k');
datetick();
xlabel('Local Time');
ylabel('N_{300} pt/cm^3');

figure(2);
plot(grimm_data(:,1),grimm_data(:,14),'-k');
datetick();
xlabel('Local Time');
ylabel('N_{1000} pt/cm^3');

figure(3);
plot(grimm_data(:,1),grimm_data(:,17),'-k');
datetick();
xlabel('Local Time');
ylabel('N_{2500} pt/cm^3');


figure(4);
plot(grimm_data(:,1),grimm_data(:,22),'-k');
datetick();
xlabel('Local Time');
ylabel('N_{10000} pt/cm^3');


