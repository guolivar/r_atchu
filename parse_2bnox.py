#!/usr/bin/env python
# Import modules
import time
import datetime
# Read settings file
settings_file = open('./2b_settings.txt')
input_file = settings_file.readline().rstrip()
output_file = settings_file.readline().rstrip()
headers = settings_file.readline().rstrip()
# If it is an invalid record ...put an invalid data line
parsed_file = open(output_file,'w')
parsed_file.write(headers + '\n')
#Plan is:
#For each line on the file:
	#check that it has a date
	#if it doesn't have a date, save to META file
	#if it does have a date:
		#separate date from rest
		#if it has TEXT ... goes to META with the timestamp
		#if it has numbers ... check what the line looks like
			#line looks like DATA, then put it in DATA file
			#line looks like O3 calibration, then put it in META file

#Problems:
#What does a "data line" looks like?
#How to identify an O3 calibration line

with open(input_file,'r') as datafile:
	for line in datafile:
		hasdate = line[0] in ["0","1","2","3","4","5","6","7","8","9","0"]
		if not hasdate: continue
		c_vec = line.split(',')
		if len(c_vec)!=12: continue
		if c_vec[1][0] == 'N': continue
		parsed_file.write(line)