#! /usr/bin/env python
# Parse messy 2B raw logging data file
# Import modules

# Go through the datafile and process lines according to what they have
with open("~/data/ATCHU/2BNOX/nox2B.txt") as datafile:
	for line in datafile:
		# get the timestamp
		if (line[0] in ["1","2","3","4","5","6","7","8","9","0"]):
			id1 = line.find(',')
			raw_line = line[id1+1:]
			timestr = line[:id1]
			# It's a DATA headers line
