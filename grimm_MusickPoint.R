# Grimm spectrometer ####
# Load packages
library('parallel')
library('ggplot2')
library('openair')
library('lubridate')
library('plot3D')
# Load data ####
grimm <- read.delim("~/data/ATCHU/MusickPoint/grimm/grimm_musick_point_fix.txt")
grimm$Second<-0
grimm$date<-ISOdatetime(grimm$Year+2000,
                        grimm$Month,
                        grimm$Day,
                        grimm$Hour,
                        grimm$Minute,
                        grimm$Second,
                        tz='GMT') + 12*3600
grimm$OctaveDate<-NULL
grimm$Year<-NULL
grimm$Month<-NULL
grimm$Day<-NULL
grimm$Hour<-NULL
grimm$Minute<-NULL
grimm$Second<-NULL
nfield=length(grimm[1,])
# Convert to particles/cc (originally as particles/litre)
grimm[,3:nfield-1] <- grimm[,3:nfield-1]/1000
Dp = as.numeric(sub("X","",names(grimm)[3:length(names(grimm))-1]))
# Green 109 units
dlogDp=c(0.0492180227,0.0299632234,0.0669467896,0.057991947,0.0511525224,0.0457574906,0.0644579892,0.0494853631,0.0321846834,0.057991947,0.096910013,0.1139433523,0.0901766303,0.096910013,0.096910013,0.079181246,0.0669467896,0.057991947,0.096910013,0.1139433523,0.0621479067,0.0543576623,0.0705810743,0.096910013,0.079181246,0.0669467896,0.057991947,0.096910013,0.079181246,0.0280287236,1)

# Deal with duplicate channels
# 108
#grimm_dndlogdp[,length(Dp)+2]<-NULL
# 109

# Plotting summaries
png("./grimm_time_series.png",width = 1200,height = 600)
timePlot(mydata = grimm,pollutant = names(grimm)[c(2,4,6,8,10)],avg.time = '1 hour')
dev.off()
png("./grimm_shades.png",width = 1200,height = 600)
image2D(data.matrix(grimm[,2:length(Dp)]),x=grimm$date,y=Dp[1:length(Dp)-1],xlab='Date',ylab='Dp',main='dN/dLogDp',log="y")
dev.off()
png("./grimm_time_variation.png",width = 1200,height = 600)
timeVariation(mydata = grimm,pollutant = names(grimm)[c(2,4,6,8,10)],normalise = TRUE)
dev.off()


